﻿namespace EnglishLearnBLL.Models
{
  public class WordViewModel
  {
    public string English { get; set; }

    public string Russian { get; set; }

    public int Level { get; set; }

    public string Example { get; set; }
  }
}
