﻿import { Word } from './word';

export class WordsCloudModel {
    UserName: string;
    TotalWords: number;
    Words: Array<Word>;
}